part of 'news_data_bloc.dart';

abstract class NewsDataEvent extends Equatable {
  final String category, country;
  const NewsDataEvent({required this.category, required this.country});
}

class GetDataNews extends NewsDataEvent {
  const GetDataNews({required super.category, required super.country});

  @override
  // TODO: implement props
  List<Object?> get props => [category, country];
}

class CleanNews extends NewsDataEvent{
  const CleanNews({required super.category, required super.country});

  @override
  // TODO: implement props
  List<Object?> get props => [];

}
