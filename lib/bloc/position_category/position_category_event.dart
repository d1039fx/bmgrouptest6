part of 'position_category_bloc.dart';

abstract class PositionCategoryEvent extends Equatable {
  final int position;
  const PositionCategoryEvent({required this.position});
}

class GetPosition extends PositionCategoryEvent{
  const GetPosition({required super.position});

  @override
  // TODO: implement props
  List<Object?> get props => [position];

}