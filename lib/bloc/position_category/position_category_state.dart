part of 'position_category_bloc.dart';

abstract class PositionCategoryState extends Equatable {
  final int position;
  const PositionCategoryState({required this.position});
}

class PositionCategoryInitial extends PositionCategoryState {
  const PositionCategoryInitial({required super.position});

  @override
  List<Object> get props => [position];
}

class PositionCategoryChange extends PositionCategoryState{
  const PositionCategoryChange({required super.position});

  @override
  // TODO: implement props
  List<Object?> get props => [position];

}