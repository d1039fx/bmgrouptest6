import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'position_category_event.dart';
part 'position_category_state.dart';

class PositionCategoryBloc extends Bloc<PositionCategoryEvent, PositionCategoryState> {
  PositionCategoryBloc() : super(const PositionCategoryInitial(position: 0)) {
    on<GetPosition>(getPosition);
  }

  void getPosition(GetPosition event, Emitter<PositionCategoryState> emitter){
    emitter(PositionCategoryChange(position: event.position));
  }
}
