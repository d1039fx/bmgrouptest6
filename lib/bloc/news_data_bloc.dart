import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:test_bmgroup_6/repository/get_data.dart';

import '../model/news_model.dart';

part 'news_data_event.dart';
part 'news_data_state.dart';

class NewsDataBloc extends Bloc<NewsDataEvent, NewsDataState> {
  GetData getData = GetData();

  NewsDataBloc() : super(const NewsDataInitial(listNews: [])) {
    on<GetDataNews>(getDataNews);
    on<CleanNews>(cleanData);
  }

  void getDataNews(GetDataNews event, Emitter<NewsDataState> emitter) async{
    await getData
        .getDataApi(country: event.country, category: event.category)
        .then ((value) {
      if (value.statusCode == 200) {
        Map<String, dynamic> data = jsonDecode(value.body);
        List dataList = data['articles'];
        List<NewsModel> listData =
            dataList.map<NewsModel>((e) => NewsModel.fromJson(e)).toList();
        emitter(NewsDataInitial(listNews: listData));
      }
    }).whenComplete(() => null).catchError((error) {
      print(error);
    });
  }

  void cleanData(CleanNews event, Emitter<NewsDataState> emitter){
    emitter(const NewsDataInitial(listNews: []));
  }
}
