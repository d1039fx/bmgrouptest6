import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';

part 'country_select_event.dart';
part 'country_select_state.dart';

class CountrySelectBloc extends Bloc<CountrySelectEvent, CountrySelectState> {
  CountrySelectBloc() : super(const CountrySelectInitial(country: '')) {
    on<GetCountry>(getCountry);
  }

  void getCountry(GetCountry event, Emitter<CountrySelectState> emitter) {
    emitter(CountrySelectInitial(country: event.country));
  }
}
