part of 'country_select_bloc.dart';

abstract class CountrySelectState extends Equatable {
  final String country;
  const CountrySelectState({required this.country});
}

class CountrySelectInitial extends CountrySelectState {
  const CountrySelectInitial({required super.country});

  @override
  List<Object> get props => [country];
}
