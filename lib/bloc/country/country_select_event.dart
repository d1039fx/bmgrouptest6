part of 'country_select_bloc.dart';

abstract class CountrySelectEvent extends Equatable {
  final String country;
  const CountrySelectEvent({required this.country});
}

class GetCountry extends CountrySelectEvent{
  const GetCountry({required super.country});

  @override
  // TODO: implement props
  List<Object?> get props => [country];

}