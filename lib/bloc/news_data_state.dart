part of 'news_data_bloc.dart';

abstract class NewsDataState extends Equatable {
  final List<NewsModel> listNews;
  const NewsDataState({required this.listNews});
}

class NewsDataInitial extends NewsDataState {
  const NewsDataInitial({required super.listNews});

  @override
  List<Object> get props => [listNews];
}
