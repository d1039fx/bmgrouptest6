import 'dart:io';

import 'package:http/http.dart' as http;

class GetData {
  Future<http.Response> getDataApi(
      {required String country, required String category}) async {
    String apiKey = '34ca464a7dc24ea1a906b1c501acb1c1';
    Uri url = Uri.https('newsapi.org', 'v2/top-headlines',
        {'country': country, 'category': category, 'apiKey': apiKey});
    http.Response response = await http.get(
      url,
      headers: <String, String>{HttpHeaders.authorizationHeader: apiKey},
    );
    return response;
  }
}
