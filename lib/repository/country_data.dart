import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;

class CountryData {
  Future<Map<String, String>> getCountryData({required String country}) async {
    Map<String, String> dataCountry = {'name':'', 'flag' :''};


    Uri url = Uri.https('restcountries.com', 'v3.1/alpha/$country');
    http.Response response = await http.get(
      url,
    );
    if (response.statusCode == 200) {
      List data = jsonDecode(response.body);
      List<Map<String, dynamic>> listData = data.map<Map<String, dynamic>>((e) => e).toList();
      dataCountry['name'] = listData[0]['name']['common'];
      dataCountry['flag'] = listData[0]['flags']['png'];
    }
    return dataCountry;
  }
}
