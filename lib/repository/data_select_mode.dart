import 'package:flutter/material.dart';

class DataSelectMode {
  final List<String> _categoryList = [
    'general',
    'business',
    'entertainment',
    'health',
    'sciences',
    'ports',
    'technology'
  ];

  final List<Map<String, dynamic>> _categoryListIcon = [
    {'category': 'general', 'icon': const Icon(Icons.travel_explore)},
    {'category': 'business', 'icon': const Icon(Icons.domain)},
    {'category': 'entertainment', 'icon': const Icon(Icons.desktop_mac)},
    {'category': 'health', 'icon': const Icon(Icons.health_and_safety)},
    {'category': 'sciences', 'icon': const Icon(Icons.science)},
    {'category': 'ports', 'icon': const Icon(Icons.portrait_sharp)},
    {'category': 'technology', 'icon': const Icon(Icons.smart_toy)},
  ];

  final List<String> _countryList = [
    'ae',
    'ar',
    'at',
    'au',
    'be',
    'bg',
    'br',
    'ca',
    'ch',
    'cn',
    'co',
    'cu',
    'cz',
    'de',
    'eg',
    'fr',
    'gb',
    'gr',
    'hk',
    'hu',
    'id',
    'ie',
    'il',
    'in',
    'it',
    'jp',
    'kr',
    'lt',
    'lv',
    'ma',
    'mx',
    'my',
    'ng',
    'nl',
    'no',
    'nz',
    'ph',
    'pl',
    'pt',
    'ro',
    'rs',
    'ru',
    'sa',
    'se',
    'sg',
    'si',
    'sk',
    'th',
    'tr',
    'tw',
    'ua',
    'us',
    've',
    'za',
  ];

  List<String> get categoryListGet => _categoryList;

  List<String> get countryListGet => _countryList;

  List<Map<String, dynamic>> get categoryListIconGet => _categoryListIcon;
}
