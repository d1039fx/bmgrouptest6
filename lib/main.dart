import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_bmgroup_6/bloc/country/country_select_bloc.dart';
import 'package:test_bmgroup_6/bloc/news_data_bloc.dart';
import 'package:test_bmgroup_6/bloc/position_category/position_category_bloc.dart';
import 'package:test_bmgroup_6/view/screens/news.dart';

void main() {
  runApp(const AppTestBMGroup());
}

class AppTestBMGroup extends StatelessWidget {
  const AppTestBMGroup({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<NewsDataBloc>(create: (context) => NewsDataBloc()),
          BlocProvider<CountrySelectBloc>(create: (context) => CountrySelectBloc()),
          BlocProvider<PositionCategoryBloc>(create: (context) => PositionCategoryBloc()),
        ],
        child: MaterialApp(
          title: 'Flutter Demo',
          theme: ThemeData(primarySwatch: Colors.blue, useMaterial3: true),
          home: const News(),
        ));
  }
}
