import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_bmgroup_6/view/screens/news_info.dart';
import 'package:transparent_image/transparent_image.dart';

import '../../bloc/news_data_bloc.dart';
import '../../model/news_model.dart';

class ListNews extends StatelessWidget {
  const ListNews({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<NewsDataBloc, NewsDataState>(
      builder: (context, state) {
        return state.listNews.isEmpty
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : ListView.builder(
                itemCount: state.listNews.length,
                itemBuilder: (context, index) {
                  NewsModel newsModel = state.listNews[index];
                  return Card(
                    margin: const EdgeInsets.all(10),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          newsModel.author == null
                              ? Text(
                                  '${index + 1}',
                                  style: const TextStyle(color: Colors.red),
                                )
                              : Row(
                                  children: [
                                    Text(
                                      '${index + 1}. ',
                                      style: const TextStyle(color: Colors.red),
                                    ),
                                    Text(
                                      '${newsModel.author}',
                                    ),
                                  ],
                                ),
                          newsModel.title == null
                              ? const SizedBox()
                              : Text(
                                  newsModel.title!,
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20),
                                ),
                          const SizedBox(
                            height: 10,
                          ),
                          newsModel.urlToImage == null
                              ? const SizedBox()
                              : ClipRRect(
                                  borderRadius: const BorderRadius.only(
                                      bottomRight: Radius.circular(50),
                                      topLeft: Radius.circular(50)),
                                  child: Image.network(newsModel.urlToImage!)),
                          const SizedBox(
                            height: 10,
                          ),
                          newsModel.description == null
                              ? const SizedBox()
                              : Text(newsModel.description!),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: Colors.red,
                                      foregroundColor: Colors.white,
                                      shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20)))),
                                  onPressed: () {},
                                  child: const Icon(Icons.star_border)),
                              const SizedBox(
                                width: 10,
                              ),
                              newsModel.content == null ? const SizedBox() : ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: Colors.blue,
                                      foregroundColor: Colors.white,
                                      shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(20)))),
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (context) => NewsInfo(
                                                image: newsModel.urlToImage!,
                                                content: newsModel.content!)));
                                  },
                                  child: const Icon(Icons.playlist_play)),
                            ],
                          )
                        ],
                      ),
                    ),
                  );
                });
      },
    );
  }
}
