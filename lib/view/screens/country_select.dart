import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_bmgroup_6/bloc/country/country_select_bloc.dart';

import 'package:test_bmgroup_6/repository/country_data.dart';
import 'package:test_bmgroup_6/repository/data_select_mode.dart';

import '../../bloc/news_data_bloc.dart';
import '../../bloc/position_category/position_category_bloc.dart';
import '../widgets/list_news.dart';
import 'category_select.dart';

class CountrySelect extends StatelessWidget with DataSelectMode {
  final CountryData countryData = CountryData();

  CountrySelect({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: GridView.builder(
          itemCount: countryListGet.length,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 4),
          itemBuilder: (context, index) {
            return FutureBuilder<Map<String, String>>(
                future:
                    countryData.getCountryData(country: countryListGet[index]),
                builder: (context, snapshot) {
                  if (snapshot.hasError) return Text(snapshot.error.toString());
                  if (snapshot.hasData) {
                    String? nameCountry = snapshot.data!['name'];
                    String? flagCountry = snapshot.data!['flag'];
                    return MaterialButton(
                      onPressed: () {
                        context
                            .read<NewsDataBloc>()
                            .add(const CleanNews(category: '', country: ''));
                        context
                            .read<CountrySelectBloc>()
                            .add(GetCountry(country: countryListGet[index]));
                        context.read<NewsDataBloc>().add(GetDataNews(
                            category: 'business',
                            country: countryListGet[index]));
                        context
                            .read<PositionCategoryBloc>()
                            .add(const GetPosition(position: 0));
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => CategorySelect()));
                      },
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          CircleAvatar(
                              backgroundImage: NetworkImage(flagCountry!)),
                          Text(nameCountry!)
                        ],
                      ),
                    );
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                });
          }),
    );
  }
}
