import 'package:flutter/material.dart';

class NewsInfo extends StatelessWidget {
  final String image, content;
  const NewsInfo({Key? key, required this.image, required this.content}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Image.network(image),
            Text(content),
          ],
        ),
      ),
    );
  }
}
