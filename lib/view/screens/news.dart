import 'package:flutter/material.dart';

import 'category_select.dart';
import 'country_select.dart';

class News extends StatelessWidget {
  const News({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('News Test App'),
      ),
      body: CountrySelect(),
    );
  }
}
