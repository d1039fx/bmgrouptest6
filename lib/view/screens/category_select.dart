import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:test_bmgroup_6/bloc/position_category/position_category_bloc.dart';
import 'package:test_bmgroup_6/repository/data_select_mode.dart';
import 'package:test_bmgroup_6/view/screens/country_select.dart';

import '../../bloc/country/country_select_bloc.dart';
import '../../bloc/news_data_bloc.dart';
import '../widgets/list_news.dart';

class CategorySelect extends StatelessWidget with DataSelectMode {
  CategorySelect({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          SizedBox(
            height: 100,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: categoryListGet.length,
                itemBuilder: (context, index) {
                  return Column(
                    children: [
                      BlocBuilder<CountrySelectBloc, CountrySelectState>(
                        builder: (context, state) {
                          int posCategoryIcon = categoryListIconGet.indexWhere(
                              (element) =>
                                  element['category'] ==
                                  categoryListGet[index]);
                          return MaterialButton(
                              child: SizedBox(
                                width: 100,
                                child: Column(
                                  children: [
                                    BlocBuilder<PositionCategoryBloc,
                                        PositionCategoryState>(
                                      builder: (context, state) {
                                        return CircleAvatar(
                                          radius: 28,
                                          foregroundColor: Colors.white,
                                          backgroundColor:
                                              categoryListGet[state.position] ==
                                                      categoryListGet[index]
                                                  ? Colors.blueAccent
                                                  : Colors.grey,
                                          child: categoryListIconGet[
                                              posCategoryIcon]['icon'],
                                        );
                                      },
                                    ),
                                    const SizedBox(
                                      height: 10,
                                    ),
                                    Text(
                                      categoryListGet[index],
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold),
                                    )
                                  ],
                                ),
                              ),
                              onPressed: () {
                                context.read<NewsDataBloc>().add(GetDataNews(
                                    category: categoryListGet[index],
                                    country: state.country));
                                context
                                    .read<PositionCategoryBloc>()
                                    .add(GetPosition(position: index));
                              });
                        },
                      )
                    ],
                  );
                }),
          ),
          const Expanded(child: ListNews())
          //SizedBox(height: 100, child: CountrySelect(category: categoryListGet[0]),)
        ],
      ),
    );
  }
}
